Ddoc

$(H4 Haber Grupları (Forumlar))

$(P
$(I Genel) ve $(I Duyuru) gruplarımız İngilizce haber gruplarının yanında:
)

$(P
<span style="margin-left:.5em;font-size:1.5em;font-family:monospace;">$(LINK http://forum.dlang.org)</span>
)

$(P
O arayüzü kullanabilmek için hesap açtırmanız gerekmiyor ama hesabınız varsa
)

$(UL
$(LI yazmış olduğunuz mesajlar yanıtlandığında bilgilendirilebiliyorsunuz, ve)
$(LI okuma geçmişiniz tarayıcınızda çerez olarak değil, sunucu tarafında saklanıyor.)
)

$(P
$(H5 Notlar:)
)
$(UL
$(LI Eski forum arşivimizdeki konular $(LINK2 https://en.wikipedia.org/wiki/BBCode, BBCode) düzeninden $(LINK2 https://en.wikipedia.org/wiki/Markdown, Markdown) düzenine dönüştürüldü ve yeni haber gruplarına aktarıldı.)

$(LI Forum arayüzünün Türkçeleştirilme işi tamamlanmak üzere.)

$(LI
$(LINK2 https://en.wikipedia.org/wiki/Network_News_Transfer_Protocol, NNTP protokolü) üzerinden erişilen haber gruplarına $(LINK2 https://www.thunderbird.net, ThunderBird) veya Outlook gibi bir email programı ile de erişebilirsiniz. Bunun için programınızı aşağıdaki gibi ayarlamanız yeterlidir:

$(UL
$(LI $(C $(B Server:) www.digitalmars.com))
$(LI $(C $(B Port:) 119))
)

$(P
O ayarları yaptıktan sonra Türkçe haber gruplarını email programınızın $(I newsgroup) olanağının $(I subscribe) seçeneği altında bulabilirsiniz:
)

$(UL
$(LI $(C digitalmars.D.turkish.genel): Her türlü konu)
$(LI $(C digitalmars.D.turkish.duyuru): Duyurular)
)
)
)
Macros:
        SUBTITLE=Haber Grupları (Forum)

        DESCRIPTION=Türkçe D programlama dili sohbet grupları

        KEYWORDS=d programlama dili forum newsgroup haber grup

       BREADCRUMBS=$(BREADCRUMBS_INDEX)
