Ddoc

$(DERS_BOLUMU $(IX const) $(IX immutable) Immutability)

$(P
Concepts are represented by the variables of a program. Interactions of concepts are commonly achieved by expressions that change the values of variables that represent those concepts. For example, the following code $(I changes) some variables that represent a purchase:
)

---
    totalPrice = calculateAmount(itemPrices);
    moneyInWallet $(HILITE -=) totalPrice;
    moneyAtMerchant $(HILITE +=) totalPrice;
---

$(P
Changing the value of a variable is called $(I modifying) or $(I mutating) that variable. Disallowing mutation of a variable is called $(I immutability).
)

$(P
As mutation is essential for most tasks, deliberately disallowing it can be counter-intuitive but is a powerful and useful feature. The concept of immutability is based on experience gained by the programming community in general: Immutability helps with correctness and maintainability of programs. This idea is so powerful that some functional programming languages disallow mutation altogether.
)

$(P
Some benefits of immutability are the following:
)

$(UL

$(LI
Some concepts are immutable by definition. For example, there are always seven days in a week, the math constant $(I pi) (π) never changes, the list of natural languages supported by a program may be fixed (e.g. only English and Turkish), etc.
)

$(LI
Even if they don't represent immutable concepts, some variables are not intended to be mutated after they are initialized; so it may be a programmer error to do so. For example, $(C totalPrice) in the code above likely should not be mutated after being assigned its initial value.
)

$(LI
By defining only some of its parameters as mutable, a function can dictate which of those parameters will be used $(I as-is) as its inputs and which parameters will be modified as side effects.
)

)

$(P
Immutability is so common in programming in general and widely adopted by D programmers that the following curiosities are accepted as consequences:
)

$(UL

$(LI
As evidenced by the programs we have been writing so far, immutability is not absolutely necessary.
)

$(LI
$(IX type qualifier) $(IX qualifier, type) The immutability concept is expressed in D by the $(C const) (short for "constant") and $(C immutable) keywords. Although the two words have the same meaning in English, their responsibilities in programs are different and they are sometimes incompatible. (Like $(LINK2 /ders/d.en/function_parameters.html, $(C inout)) and $(LINK2 /ders/d.en/concurrency_shared.html, $(C shared)), $(C const) and $(C immutable) are $(I type qualifiers).)
)


$(LI
Both of the terms "immutable variable" and "constant variable" are nonsensical when the word "variable" is taken literally to mean $(I something that changes). In a broader sense, the word "variable" is often understood to mean any concept of a program, either mutable or immutable.
)

$(LI
As an example of self-fulfilling prophecy, functions are forced to observe immutability (forced to be $(I const-correct)) and become more useful as a consequence. (This forced observance of const-correctness is sometimes described as being similar to a viral disease.)
)

)


$(H5 Immutable variables)

$(P
There are three ways of defining variables that cannot be mutated.
)

$(H6 $(IX enum) $(C enum) constants)

$(P
We have seen earlier in the $(LINK2 /ders/d.en/enum.html, $(C enum) chapter) that $(C enum) defines named constant values:
)

---
    enum fileName = "list.txt";
---

$(P
As long as their values can be determined at compile time, $(C enum) variables can be initialized with more complex expressions as well, including return values of functions:
)

---
int totalLines() {
    return 42;
}

int totalColumns() {
    return 7;
}

string name() {
    return "list";
}

void main() {
    enum fileName $(HILITE =) name() ~ ".txt";
    enum totalSquares $(HILITE =) totalLines() * totalColumns();
}
---

$(P
The D feature that enables such initialization is $(I compile time function execution) (CTFE), which we will see in $(LINK2 /ders/d.en/functions_more.html, a later chapter).
)


$(P
As expected, the values of $(C enum) constants cannot be modified:
)

---
    ++totalSquares;    $(DERLEME_HATASI)
---

$(P
Although it is a very effective way of representing immutable values, $(C enum) can only be used with compile-time values.
)

$(P
An $(C enum) constant is $(I a manifest constant), meaning that the program is compiled as if every mention of that constant had been replaced by its value. As an example, let's consider the following $(C enum) definition and the two expressions that make use of it:
)

---
    enum i = 42;
    writeln(i);
    foo(i);
---

$(P
The code above is the exact equivalent of the one below, where every use of $(C i) is replaced with its value of $(C 42):
)

---
    writeln(42);
    foo(42);
---

$(P
$(IX enum, array cost) $(IX array, cost of enum) Although that replacement makes sense for simple types like $(C int) and makes no difference to the resulting program, $(C enum) constants can bring a hidden cost when they are used for arrays or associative arrays:
)

---
    enum a = [ 42, 100 ];
    writeln(a);
    foo(a);
---

$(P
After replacing $(C a) with its value, the equivalent code that the compiler would be compiling is the following:
)

---
    writeln([ 42, 100 ]); // an array is created at run time
    foo([ 42, 100 ]);     // another array is created at run time
---

$(P
The hidden cost here is that there would be two separate arrays created for the two expressions above. For that reason, it makes more sense to define arrays and associative arrays as $(C immutable) variables if they are going to be mentioned more than once in the program.
)

$(H6 $(IX variable, const) $(C const) variables)

$(P
Like $(C enum), this keyword specifies that the value of a variable will never change. Unlike $(C enum), a $(C const) variable is an actual variable with a memory address and $(C const) variables are commonly initialized during the execution of the program.
)

$(P
The compiler does not allow mutating a $(C const) variable:
)

---
    $(HILITE const) half = total / 2;
    half = 10;    $(DERLEME_HATASI)
---

$(P
The following program uses both $(C enum) and $(C const). The program asks for the user to guess a number that has been picked randomly. Since the random number cannot be determined at compile time, it cannot be defined as an $(C enum). Still, since the randomly picked value must never be changed after having been decided, it is suitable to specify that variable as $(C const).
)

$(P
The program takes advantage of the $(C readInt()) function that was defined in the previous chapter:
)

---
import std.stdio;
import std.random;

int readInt(string message) {
    int result;
    write(message, "? ");
    readf(" %s", &result);
    return result;
}

void main() {
    enum min = 1;
    enum max = 10;

    $(HILITE const) number = uniform(min, max + 1);

    writefln("I am thinking of a number between %s and %s.",
             min, max);

    auto isCorrect = false;
    while (!isCorrect) {
        $(HILITE const) guess = readInt("What is your guess");
        isCorrect = (guess == number);
    }

    writeln("Correct!");
}
---

$(P
Observations:
)

$(UL

$(LI $(C min) and $(C max) are integral parts of the behavior of this program and their values are known at compile time. For that reason they are defined as $(C enum) constants.
)

$(LI $(C number) is specified as $(C const) because it would not be appropriate to modify it after its initialization at run time. Likewise for each user guess: once read, the guess should not be modified.
)

$(LI The types of those variables are not specified explicitly. As with the uses of $(C auto), $(C enum), etc.; the type of a $(C const) variable can be inferred from the expression on the right hand side.
)

)

$(P
Although it is not necessary to write the type fully, $(C const) normally takes the actual type within parentheses, e.g. $(C const(int)). The output of the following program demonstrates that the full names of the types of the three variables are in fact the same:
)

---
import std.stdio;

void main() {
    const      inferredType = 0;
    const int  explicitType = 1;
    const(int) fullType     = 2;

    writeln(typeof(inferredType).stringof);
    writeln(typeof(explicitType).stringof);
    writeln(typeof(fullType).stringof);
}
---

$(P
The actual name of the type includes $(C const):
)

$(SHELL
const(int)
const(int)
const(int)
)

$(P
The use of parentheses has significance, and specifies which parts of the type are immutable. We will see this below when discussing the immutability of an entire slice vs. its elements.
)

$(H6 $(IX variable, immutable) $(C immutable) variables)

$(P
When defining variables the $(C immutable) keyword has the same effect as $(C const). $(C immutable) variables cannot be modified:
)

---
    $(HILITE immutable) half = total / 2;
    half = 10;    $(DERLEME_HATASI)
---

$(P
Unless other parts of the program require a variable to be $(C immutable), immutable variables can either be defined as $(C const) or $(C immutable). When a function requires specifically that a parameter must be $(C immutable), then a variable corresponding to that parameter must be defined as $(C immutable). We will see this below.
)

$(H5 Parameters)

$(P
As we will see in the next two chapters, functions can mutate their parameters. For example, they can mutate the elements of slices that are passed as arguments to those functions.
)

$(P
As you would remember from the $(LINK2 /ders/d.en/slices.html, Slices and Other Array Features chapter), slices do not own elements but provide access to them. There may be more than one slice at a given time that provides access to the same elements.
)

$(P
Although the examples in this section focus only on slices, this topic is applicable to associative arrays and classes as well because they too are $(I reference types).
)

$(P
A slice that is passed as a function argument is not the slice that the function is called with. The argument is a $(I copy) of the slice variable. (Only the slice $(I variable) is copied, not the elements.)
)

---
import std.stdio;

void main() {
    int[] slice = [ 10, 20, 30, 40 ];  // 1
    halve(slice);
    writeln(slice);
}

void halve(int[] numbers) {            // 2
    foreach (ref number; numbers) {
        number /= 2;
    }
}
---

$(P
When program execution enters the $(C halve()) function, there are two slices that provide access to the same four elements:
)

$(OL

$(LI The slice named $(C slice) that is defined in $(C main()), which is passed to $(C halve()) as its parameter
)

$(LI The slice named $(C numbers) that $(C halve()) receives as its argument, which provides access to the same elements as $(C slice)
)

)

$(P
Since both slices refer to the same elements and that we use the $(C ref) keyword in the $(C foreach) loop, the values of the elements get halved:
)

$(SHELL
[5, 10, 15, 20]
)

$(P
It is indeed useful for functions to be able to modify the elements of the slices that are passed as arguments. As we have seen in this example, some functions exist just for that purpose.
)

$(P
The compiler does not allow passing $(C const) variables as arguments to such functions:
)

---
    $(HILITE const)(int[]) slice = [ 10, 20, 30, 40 ];
    halve(slice);    $(DERLEME_HATASI)
---

$(P
The compilation error indicates that a variable of type $(C const(int[])) cannot be used as an argument of type $(C int[]):
)

$(SHELL
Error: function deneme.halve ($(HILITE int[]) numbers) is not callable
using argument types ($(HILITE const(int[])))
)

$(H6 $(IX parameter, const) $(C const) parameters)

$(P
It is important and natural that $(C const) variables be prevented from being passed to functions like $(C halve()) that modify their arguments. However, it would be a limitation if they could not be passed to functions that do not intend to modify them like the $(C print()) function below:
)

---
import std.stdio;

void main() {
    const(int[]) slice = [ 10, 20, 30, 40 ];
    print(slice);    $(DERLEME_HATASI)
}

void print(int[] slice) {
    writefln("%s elements: ", slice.length);

    foreach (i, element; slice) {
        writefln("%s: %s", i, element);
    }
}
---

$(P
It does not make sense above that a slice is prevented from being printed just because it is $(C const). The proper way of dealing with this situation is using $(C const) parameters. This is called making the function $(I const-correct). (This is the self-fulfilling prophecy mentioned above that forces functions to observe immutability.)
)

$(P
The $(C const) keyword specifies that a variable is not modified through $(I that particular reference) (e.g. a slice) of that variable. Specifying a parameter as $(C const) guarantees that the elements of the slice are not modified inside the function. Once $(C print()) provides this guarantee, the program can now be compiled:
)

---
    print(slice);    // now compiles
// ...
void print($(HILITE const) int[] slice) {
    // ...
}
---

$(P
This guarantee of non-mutation provides flexibility because it allows passing $(I mutable), $(C const), and $(C immutable) variables as arguments:
)

---
    $(HILITE int[]) mutableSlice = [ 7, 8 ];
    print(mutableSlice);    // compiles

    $(HILITE const int[]) slice = [ 10, 20, 30, 40 ];
    print(slice);           // compiles

    $(HILITE immutable int[]) immSlice = [ 1, 2 ];
    print(immSlice);        // compiles
---

$(P
$(IX const-correct) $(IX viral, immutability) Conversely, failing to define a parameter as $(C const) when that parameter is not modified in the function reduces the applicability of that function. Such functions are not const-correct.
)

$(P
Another benefit of $(C const) parameters is providing useful information to the programmer: Knowing that a variable will not be modified when passed to a function makes the code easier to understand.
)

$(P
The fact that $(C const) parameters can accept $(I mutable), $(C const), and $(C immutable) variables has an interesting consequence. This is explained in the "Should a parameter be $(C const) or $(C immutable)?" section below.
)

$(H6 $(IX in, immutability) $(C in) parameters)

$(P
As we will see in the next chapter, $(C in) implies $(C const) and is more useful with the $(C $(HYPHEN)preview=in) command line switch. For that reason, I recommend $(C in) parameters over $(C const) parameters.
)

$(H6 $(IX parameter, immutable) $(C immutable) parameters)

$(P
$(C const) parameters can be seen as $(I welcoming) because they accept $(I mutable), $(C const), and $(C immutable) variables as arguments.
)

$(P
In contrast, $(C immutable) parameters are $(I selective) because they bring a strong requirement: The argument must be $(C immutable). While a $(C const) parameter communicates "I will not mutate", an $(C immutable) parameter adds "and you should not mutate either".
)

$(P
Only $(C immutable) variables can be passed to functions as their $(C immutable) parameters:
)

---
void func($(HILITE immutable) int[] slice) {
    // ...
}

void main() {
    immutable int[] immSlice = [ 1, 2 ];
              int[]    slice = [ 8, 9 ];

    func(immSlice);      // compiles
    func(slice);         $(DERLEME_HATASI)
}
---

$(P
For that reason, the $(C immutable) specifier should be used only when this requirement is actually necessary. We have indeed been using the $(C immutable) specifier indirectly through certain string types. This will be covered below.
)

$(P
We have seen that the parameters that are specified as $(C const) or $(C immutable) promise not to modify $(I the actual variable) that is passed as an argument. This is relevant only for reference types because only then there is $(I the actual variable) to talk about the immutability of.
)

$(P
$(I Reference types) and $(I value types) will be covered in the next chapter. Among the types that we have seen so far, only slices and associative arrays are reference types; the others are value types.
)

$(H6 $(IX parameter, const vs. immutable) Should a parameter be $(C const) or $(C immutable)?)

$(P
$(I $(B Note:) As $(C in) implies $(C const), this section is about $(C in) as well.
)
)

$(P
The sections above may give the impression that, being more flexible, $(C const) parameters should be preferred over $(C immutable) parameters. This is not always true.
)

$(P
$(IX erase, const) $(C const) $(I erases) the information of whether the original variable was $(I mutable), $(C const), or $(C immutable). This information is hidden even from the compiler.
)

$(P
A consequence of this fact is that $(C const) parameters cannot be passed as arguments to functions that take $(C immutable) parameters. For example, the intermediate function $(C foo()) below cannot pass its $(C const) parameter to $(C bar()) even though the actual variable that is passed through the functions is defined as $(C immutable) to begin with in $(C main):
)

---
void main() {
    /* The original variable is immutable */
    $(HILITE immutable) int[] slice = [ 10, 20, 30, 40 ];
    foo(slice);
}

/* A function that takes its parameter as const, in order to
 * be more useful. */
void foo(const int[] slice) {
    bar(slice);    $(DERLEME_HATASI)
}

/* A function that requires an immutable slice. */
void bar(immutable int[] slice) {
    // ...
}
---

$(P
$(C bar()) requires the parameter to be $(C immutable). However, it is not known (in general) whether the original variable that $(C foo())'s $(C const) parameter references was $(C immutable) or not.
)

$(P
$(I $(B Note:) It is clear to an observer in the code above that the original variable in $(C main()) is $(C immutable). However, the compiler compiles functions individually, without regard to every place that function is called from. To the compiler, the $(C slice) parameter of $(C foo()) may refer to a mutable variable or an $(C immutable) one.
)
)

$(P
A solution would be to call $(C bar()) with an immutable copy of the parameter:
)

---
void foo(const int[] slice) {
    bar(slice$(HILITE .idup));
}
---

$(P
Although that would make the code compile, it does incur into the cost of copying the slice and its contents, which would be wasteful in the case where the original variable was $(C immutable) to begin with.
)

$(P
After this analysis, it should be clear that always declaring parameters as $(C const) is not the best approach in every situation. After all, if $(C foo())'s parameter had been defined as $(C immutable) there would be no need to copy it before calling $(C bar()):
)

---
void foo(immutable int[] slice) {  // This time immutable
    bar(slice);    // Copying is not needed anymore
}
---

$(P
Although the code compiles, defining the parameter as $(C immutable) has a similar cost: This time an $(C immutable) copy of the original variable is needed when calling $(C foo()), if that variable was not $(C immutable) to begin with:
)

---
    foo(mutableSlice$(HILITE .idup));
---

$(P
Templates can help. (We will see templates in later chapters.) Although I don't expect you to fully understand the following function at this point in the book, I will present it as a solution to this problem. The following function template $(C foo()) can be called with $(I mutable), $(C const), and $(C immutable) variables. The parameter would be copied only if the original variable was mutable; no copying would take place if it were $(C immutable):
)

---
import std.conv;
// ...

/* Because it is a template, foo() can be called with both mutable
 * and immutable variables. */
void foo(T)(T[] slice) {
    /* 'to()' does not make a copy if the original variable is
     * already immutable. */
    bar(to!(immutable T[])(slice));
}
---

$(H5 $(IX initialization, immutability) Initialization)

$(P
Disallowing mutations can be seen as a limitation when initial values of variables depend on non-trivial expressions. For example, the contents of the $(C fruits) array below depend on the value of $(C addCitrus) but the code fails to compile because the variable is $(C const):
)

---
    $(HILITE const) fruits = [ "apple", "pear" ];

    if (addCitrus) {
        fruits ~= [ "orange" ];    $(DERLEME_HATASI)
    }
---

$(P
Although making the variable mutable e.g. by defining it with $(C auto) would allow the code to compile, it is still possible to define it as $(C const) by moving the initialization code to a function:
)

---
bool addCitrus;

string[] makeFruits() {
    auto result = [ "apple", "pear" ];

    if (addCitrus) {
        result ~= [ "orange" ];
    }

    return result;
}

void main() {
    $(HILITE const) fruits = $(HILITE makeFruits());
}
---

$(P
Note how the local $(C result) array is mutable but the $(C fruits) is still $(C const) (presumably as desired by the programmer). When it is impossible or cumbersome to move the code to a named function, a $(LINK2 /ders/d.en/lambda.html, lambda) can be used instead:
)

---
    const fruits = $(HILITE {)
      // Exactly the same code as 'makeFruits()'.
      auto result = [ "apple", "pear" ];

      if (addCitrus) {
          result ~= [ "orange" ];
      }

      return result;
    $(HILITE })();
---

$(P
The lambda is defined by the highlighted curly braces and is executed by the parenteses at the end. Again, the $(C fruits) variable ends up being $(C const) as desired.
)

$(P
$(IX shared static this, immutability) D allows assigning to $(C const) and $(C immutable) variables in special initialization blocks called $(C shared static this()) (and $(C static this())). These blocks are for initializing variables defined at module scope (outside of any function). It is possible to mutate $(C const) and $(C immutable) variables in $(C shared static this()) blocks:
)

---
immutable int[] i;

shared static this() {
    // It is possible to mutate 'const' and 'immutable' module
    // variables in this block:
    i ~= 43;

    // The variables are still 'const' and 'immutable' for the
    // rest of the program.
}
---

$(P
$(C shared static this()) blocks are executed before the program  starts running the body of the $(C main()) function.
)

$(H5 Immutability of the slice versus the elements)

$(P
We have seen above that the type of a $(C const) slice has been printed as $(C const(int[])). As the parentheses after $(C const) indicate, it is the entire slice that is $(C const). Such a slice cannot be modified in any way: elements may not be added or removed, their values may not be modified, and the slice may not start providing access to a different set of elements:
)

---
    const int[] slice = [ 1, 2 ];
    slice ~= 3;               $(DERLEME_HATASI)
    slice[0] = 3;             $(DERLEME_HATASI)
    slice.length = 1;         $(DERLEME_HATASI)

    const int[] otherSlice = [ 10, 11 ];
    slice = otherSlice;       $(DERLEME_HATASI)
---

$(P
Taking immutability to that extreme may not be suitable in every case. In most cases, what is important is the immutability of the elements themselves. Since a slice is just a tool to access the elements, it should not matter if we make changes to the slice itself as long as the elements are not modified. This is especially true in the cases we have seen so far, where the function receives a copy of the slice itself.
)

$(P
To specify that only the elements are immutable we use the $(C const) keyword with parentheses that enclose just the element type. Modifying the code accordingly, now only the elements are immutable, not the slice itself:
)

---
    const$(HILITE (int))[] slice = [ 1, 2 ];
    slice ~= 3;               // can add elements
    slice[0] = 3;             $(DERLEME_HATASI)
    slice.length = 1;         // can drop elements

    const int[] otherSlice = [ 10, 11 ];
    slice = otherSlice;       /* can provide access to
                               * other elements */
---

$(P
Although the two syntaxes are very similar, they have different meanings. To summarize:
)

---
    const int[]  a = [1]; /* Neither the elements nor the
                           * slice can be modified */

    const(int[]) b = [1]; /* The same meaning as above */

    const(int)[] c = [1]; /* The elements cannot be
                           * modified but the slice can be */
---

$(P
This distinction has been in effect in some of the programs that we have written so far. As you may remember, the three string aliases involve immutability:
)

$(UL
$(LI $(C string) is an alias for $(C immutable(char)[]))
$(LI $(C wstring) is an alias for $(C immutable(wchar)[]))
$(LI $(C dstring) is an alias for $(C immutable(dchar)[]))
)

$(P
Likewise, string literals are immutable as well:
)

$(UL
$(LI The type of literal $(STRING "hello"c) is $(C string))
$(LI The type of literal $(STRING "hello"w) is $(C wstring))
$(LI The type of literal $(STRING "hello"d) is $(C dstring))
)

$(P
According to these definitions, D strings are normally arrays of $(I $(C immutable) characters).
)

$(H6 $(IX transitive, immutability) $(C const) and $(C immutable) are transitive)

$(P
As mentioned in the code comments of slices $(C a) and $(C b) above, both those slices and their elements are $(C immutable).
)

$(P
This is true for $(LINK2 /ders/d.en/struct.html, structs) and $(LINK2 /ders/d.en/class.html, classes) as well, both of which will be covered in later chapters. For example, all members of a $(C const) $(C struct) variable are $(C const) and all members of an $(C immutable) $(C struct) variable are $(C immutable). (Likewise for classes.)
)

$(H6 $(IX .dup) $(IX .idup) $(C .dup) and $(C .idup))

$(P
There may be mismatches in immutability when strings are passed to functions as parameters. The $(C .dup) and $(C .idup) properties make copies of arrays with the desired mutability:
)

$(UL
$(LI $(C .dup) makes a mutable copy of the array ("dup" stands for "do duplicate"))
$(LI $(C .idup) makes an immutable copy of the array)
)

$(P
For example, a function that insists on the immutability of a parameter may have to be called with an immutable copy of a mutable string:
)

---
void foo($(HILITE string) s) {
    // ...
}

void main() {
    char[] salutation;
    foo(salutation);                $(DERLEME_HATASI)
    foo(salutation$(HILITE .idup));           // ← this compiles
}
---

$(H5 How to use)

$(UL

$(LI
As a general rule, prefer immutable variables over mutable ones.
)

$(LI
Define constant values as $(C enum) if their values can be calculated at compile time. For example, the constant value of $(I seconds per minute) can be an $(C enum):

---
    enum int secondsPerMinute = 60;
---

$(P
There is no need to specify the type explicitly if it can be inferred from the right hand side:
)

---
    enum secondsPerMinute = 60;
---

)

$(LI
Consider the hidden cost of $(C enum) arrays and $(C enum) associative arrays. Define them as $(C immutable) variables if they are used more than once in the program.
)

$(LI
Specify variables as $(C const) if their values will never change but cannot be known at compile time. Again, the type can be inferred:

---
    const guess = readInt("What is your guess");
---

)

$(LI
If a function does not modify a parameter, specify that parameter as $(C in). This would express programmer's intent and allow both mutable and $(C immutable) variables to be passed as arguments:

---
void foo(in char[] s) {
    // ...
}

void main() {
    char[] mutableString;
    string immutableString;

    foo(mutableString);      // ← compiles
    foo(immutableString);    // ← compiles
}
---

)

$(LI
Unlike most examples throughout this book, always specify what part of a parameter should be immutable:

---
// Only the elements may not be mutated (the slice can be)
void print_1(const$(HILITE $(PARANTEZ_AC))int$(HILITE $(PARANTEZ_KAPA))[] slice) {
    // ...
}

// Neither the slice variable nor the elements can be mutated
void print_2(const$(HILITE $(PARANTEZ_AC))int[]$(HILITE $(PARANTEZ_KAPA)) slice) {
    // ...
}

// Same as print_2()  (Unlike most examples in this book, avoid this style)
void print_3(const int[] slice) {
    // ...
}
---
)

$(LI
Consider that $(C const) parameters cannot be passed to functions taking $(C immutable). See the section "Should a parameter be $(C const) or $(C immutable)?" above.
)

$(LI
If the function modifies a parameter, leave that parameter as mutable ($(C in), $(C const), or $(C immutable) would not allow modifications anyway):

---
import std.stdio;

void reverse(dchar[] s) {
    foreach (i; 0 .. s.length / 2) {
        immutable temp = s[i];
        s[i] = s[$ - 1 - i];
        s[$ - 1 - i] = temp;
    }
}

void main() {
    dchar[] salutation = "hello"d.dup;
    reverse(salutation);
    writeln(salutation);
}
---

$(P
The output:
)

$(SHELL
olleh
)

)

)

$(H5 Summary)

$(UL

$(LI $(C enum) variables represent immutable concepts that are known at compile time.)

$(LI $(C enum) arrays and associative arrays have the cost of creating a new array at every mention of that $(C enum) constant. Prefer $(C immutable) for arrays and associative arrays instead.)

$(LI Prefer $(C in) parameters over $(C const) parameters.)

$(LI $(C const) and $(C immutable) variables represent immutable concepts that can be known at run time (or somewhat obscurely, that must have some memory location that we can refer to).)

$(LI $(C const) parameters are the ones that functions do not modify. $(I Mutable), $(C const), and $(C immutable) variables can be passed as arguments of $(C const) parameters.)

$(LI $(C immutable) parameters are the ones that functions specifically require them to be so. Only $(C immutable) variables can be passed as arguments of $(C immutable) parameters.)

$(LI $(C const(int[])) specifies that neither the slice nor its elements can be modified.)

$(LI $(C const(int)[]) specifies that only the elements cannot be modified.)

)

Macros:
        SUBTITLE=Immutability

        DESCRIPTION=The const and immutable keywords of D, which support the concept of immutability.

        KEYWORDS=d programming language tutorial book immutable const
