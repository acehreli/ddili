Ddoc

$(DERS_BOLUMU $(IX type) $(IX fundamental type) Fundamental Types)

$(P
We have seen that the brain of a computer is the CPU. Most of the tasks of a program are performed by the CPU and the rest are dispatched to other parts of the computer.
)

$(P
The smallest unit of data in a computer is called $(I a bit). The value of a bit can be either 0 or 1.
)

$(P
Since a type of data that can hold only the values 0 and 1 would have very limited use, the CPU supports larger data types that are combinations of more than one bit. As an example, a $(I byte) usually consists of 8 bits. If an N-bit data type is the most efficient data type supported by a CPU, we consider it to be an $(I N-bit CPU): as in 32-bit CPU, 64-bit CPU, etc.
)

$(P
The data types that the CPU supports are still not sufficient: they can't represent higher level concepts like $(I name of a student) or $(I a playing card). Likewise, D's fundamental data types are not sufficient to represent many higher level concepts. Such concepts must be defined by the programmer as $(I structs) and $(I classes) by taking advantage of fundamental types. We will see structs and classes in later chapters.
)

$(P
D's $(I fundamental types) are similar to the fundamental types of many other languages. We will see more information about these types in the upcoming chapters.
)

$(H5 $(IX bool) Logical expression type)

$(P
This is the type that is used in logical expressions. It takes the value of $(C true) for truth and $(C false) for falsity:
)

$(TABLE $(TABLE_SIZE),
$(HEAD3 Type, Definition, Initial Value)
$(ROW3  $(C bool), Boolean type, $(C false))
)

$(H5 $(IX byte) $(IX ubyte) $(IX short) $(IX ushort) $(IX int) $(IX uint) $(IX long) $(IX ulong) Integer types)

$(P
Integers are numbers that don't have fractional parts. For example, 3 is an integer but 3.5 is not.
)

$(P
Types that can have negative and positive values are $(I signed) types. The names of these types come from the negative $(I sign). Types that can have only positive values are $(I unsigned) types. The $(C u) at the beginning of the name of these types comes from $(I unsigned).
)

$(TABLE $(TABLE_SIZE),
$(HEAD3 Type, Definition, Initial Value)
$(ROW3 $(C byte), signed 8 bits, $(C 0))
$(ROW3 $(C ubyte), unsigned 8 bits, $(C 0))
$(ROW3 $(C short), signed 16 bits, $(C 0))
$(ROW3 $(C ushort), unsigned 16 bits, $(C 0))
$(ROW3 $(C int), signed 32 bits, $(C 0))
$(ROW3 $(C uint), unsigned 32 bits, $(C 0))
$(ROW3 $(C long), signed 64 bits, $(C 0L))
$(ROW3 $(C ulong), unsigned 64 bits, $(C 0LU))
)

$(H5 $(IX float) $(IX double) $(IX real) $(IX .nan) Floating point types)

$(P
Floating point types are the types that can represent values with fractions as in 1.25. The precision of floating point calculations are directly related to the bit count of the type: higher the bit count, more precise the results are. Only floating point types can represent fractions; integer types like $(C int) can only represent whole values like 1 and 2.
)

$(P
Unlike integers, there are special values for floating point types that represent invalid values. These special values are written as properties as $(C .nan) and are short for "not a number".
)

$(TABLE $(TABLE_SIZE),
$(HEAD3 Type, Definition, Initial Value)
$(ROW3 $(C float), 32 bits, $(C float.nan))
$(ROW3 $(C double), 64 bits, $(C double.nan))
$(ROW3 $(C real), at least 64 bits but can be more$(BR)if the hardware provides more, $(C real.nan))
)


$(H5 $(IX char) $(IX wchar) $(IX dchar) Character types)

$(P
These types are used for representing letters of alphabets and other symbols of writing systems:
)

$(TABLE $(TABLE_SIZE),
$(HEAD3 Type, Definition, Initial Value)
$(ROW3 $(C char), UTF-8 code unit, $(C 0xFF))
$(ROW3 $(C wchar), UTF-16 code unit, $(C 0xFFFF))
$(ROW3 $(C dchar), UTF-32 code unit$(BR)and Unicode code point, $(C 0x0000FFFF))
)

$(P
In addition to the above, the keyword $(C void) represents $(I having no type).
)

$(P
The keywords $(C cent) and $(C ucent) are reserved for future use to represent signed and unsigned 128 bit values.
)

$(P
Unless there is a reason not to, you can use $(C int) to represent whole values. To represent concepts that can have fractional values, consider $(C double).
)

$(H5 Properties of types)

$(P
D types have $(I properties) that provide information about these types. Properties are accessed with a dot after the name of the type. For example, the $(C sizeof) property of $(C int) is accessed as $(C int.sizeof). We will see only some of type properties in this chapter:
)

$(UL

$(LI $(IX .stringof) $(C .stringof) is the name of the type)

$(LI $(IX .sizeof) $(C .sizeof) is the length of the type in terms of bytes. (In order to determine the bit count, this value must be multiplied by 8, the number of bits in a $(C byte).)
)

$(LI $(IX .min) $(C .min) is short for "minimum"; this is the smallest value that the type can have)

$(LI $(IX .max) $(C .max) is short for "maximum"; this is the largest value that the type can have)

$(LI $(IX .init) $(IX initial value) $(IX default value, type) $(C .init) is short for "initial value" (default value); this is the value that D assigns to a type when an initial value is not specified)

)

$(P
Here is a program that prints these properties for $(C int):
)

---
import std.stdio;

void main() {
    writeln("Type           : ", int.stringof);
    writeln("Length in bytes: ", int.sizeof);
    writeln("Minimum value  : ", int.min);
    writeln("Maximum value  : ", int.max);
    writeln("Initial value  : ", int.init);
}
---

$(P
The output of the program is the following:
)

$(SHELL
Type           : int
Length in bytes: 4
Minimum value  : -2147483648
Maximum value  : 2147483647
Initial value  : 0
)

$(H5 $(IX size_t) $(C size_t))

$(P
You will come across the $(C size_t) type as well. $(C size_t) is not a separate type but an alias of an existing unsigned type. Its name comes from "size type". It is the most suitable type to represent concepts like $(I size) or $(I count).
)

$(P
$(C size_t) is large enough to represent the number of bytes of the memory that a program can potentially be using. Its actual size depends on the system: $(C uint) on a 32-bit system and $(C ulong) on a 64-bit system. For that reason, $(C ulong) is larger than $(C size_t) on a 32-bit system.
)

$(P
You can use the $(C .stringof) property to see what $(C size_t) is an alias of on your system:
)

---
import std.stdio;

void main() {
    writeln(size_t.stringof);
}
---

$(P
The output of the program is the following on my system:
)

$(SHELL
ulong
)

$(PROBLEM_TEK

$(P
Print the properties of other types. Notes:
)

$(UL
$(LI You can't use the reserved types $(C cent) and $(C ucent) in any program.)

$(LI $(C void) does not have the properties $(C .min), $(C .max) and $(C .init).)

$(LI $(C .min), $(C .max), and $(C .init) values of character types do not have visible representations that you can see on the output.)

$(LI The $(C .min) property cannot be used for floating point types. (You can see all the various properties for the fundamental types in the $(LINK2 http://dlang.org/property.html, D property specification)). As we will see later in $(LINK2 /ders/d.en/floating_point.html, the Floating Point Types chapter), you must use the negative of the $(C .max) property e.g. as $(C -double.max) instead.)
)

)


Macros:
        SUBTITLE=Fundamental Types

        DESCRIPTION=The fundamental types of the D programming language

        KEYWORDS=d programming language tutorial book fundamental types numeric limits

TABLE_SIZE=wide
