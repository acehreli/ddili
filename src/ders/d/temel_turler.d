Ddoc

$(DERS_BOLUMU $(IX tür) $(IX tip) $(IX temel tür) Temel Türler)

$(P
Bir bilgisayarın beyninin $(I mikro işlemci) olduğunu gördük. Bir programdaki işlemlerin çoğunu mikro işlemci işletir. Kendi yapamayacağı işleri ise bilgisayarın yan birimlerine devreder.
)

$(P
Bilgisayarlarda en küçük bilgi birimi, 0 veya 1 değerini tutabilen ve $(I bit) adı verilen yapıdır.
)

$(P
Yalnızca 0 ve 1 değerini tutabilen bir veri türünün kullanımı çok kısıtlı olduğundan, mikro işlemciler birden fazla bitin yan yana getirilmesinden oluşan daha kullanışlı veri türleri tanımlamışlardır: örneğin 8 bitten oluşan $(I bayt) veya 32, 64, vs. bitten oluşan daha büyük veri türleri. Eğer türlerden N bitlik olanı bir mikro işlemcinin en etkin olarak kullandığı tür ise, o mikro işlemcinin $(I N bitlik) olduğu söylenir: "32 bitlik işlemci" veya "64 bitlik işlemci" gibi.
)

$(P
Mikro işlemcinin tanımladığı veri türleri de kendi başlarına yeterli değillerdir çünkü örneğin $(I öğrenci ismi) gibi veya $(I oyun kağıdı) gibi özel bilgileri tutamazlar. Mikro işlemcinin sunduğu bu genel amaçlı veri türlerini daha kullanışlı türlere çevirmek programlama dillerinin görevidir. Örneğin, $(I oyun kağıdı) gibi kavramlar, ileride anlatılacak olan $(I yapılarla) ve $(I sınıflarla) ama yine de temel türlerden yararlanarak ifade edilirler.
)

$(P
D'nin temel türleri diğer dillerin temel türlerine benzerler. Bu türlerin ayrıntılarını sonraki bölümlerde göreceğiz.
)

$(H5 $(IX bool) Mantıksal ifade türü)

$(P
Mantıksal ifadelerde kullanılan ve "doğruluk" durumunda $(C true), "doğru olmama" durumunda $(C false) değerini alan türdür:
)


$(TABLE $(TABLE_SIZE),
$(HEAD3 Tür, Açıklama, İlk Değer)
$(ROW3  $(C bool), Bool değeri, $(C false))
)

$(H5 $(IX byte) $(IX ubyte) $(IX short) $(IX ushort) $(IX int) $(IX uint) $(IX long) $(IX ulong) Tamsayı türleri)

$(P
Tamsayılar, küsuratı olmayan sayılardır. Örneğin, 3 bir tamsayıdır ama 3.5 değildir. (Not: Çoğu programlama dilinde olduğu gibi, kesirler D dilinde de Türkçe yazım kurallarından farklı olarak virgülle değil, nokta ile ayrılır. O yüzden, normalde 3,5 olarak yazılması gereken değerleri metin içinde bile 3.5 olarak yazacağım.)
)

$(P
Hem eksi hem artı değer alabilen türler $(I işaretli) türlerdir. İsimleri eksi $(I işaretinden) gelir. Yalnızca artı değerler alabilen türler ise $(I işaretsiz) türlerdir. Bu türlerin isimlerinin başındaki $(C u) harfi, "işaretsiz" anlamına gelen "unsigned"ın baş harfidir.
)

$(TABLE $(TABLE_SIZE),
$(HEAD3 Tür, Açıklama, İlk Değer)
$(ROW3 $(C byte), işaretli 8 bit, $(C 0))
$(ROW3 $(C ubyte), işaretsiz 8 bit, $(C 0))
$(ROW3 $(C short), işaretli 16 bit, $(C 0))
$(ROW3 $(C ushort), işaretsiz 16 bit, $(C 0))
$(ROW3 $(C int), işaretli 32 bit, $(C 0))
$(ROW3 $(C uint), işaretsiz 32 bit, $(C 0))
$(ROW3 $(C long), işaretli 64 bit, $(C 0L))
$(ROW3 $(C ulong), işaretsiz 64 bit, $(C 0LU))
)

$(H5 $(IX float) $(IX double) $(IX real) $(IX .nan) Kesirli sayı türleri)

$(P
$(I Kayan noktalı) diye de anılan ve 1.25 gibi kesirli değerleri tutabilen türlerdir. Hesapların hassasiyeti türlerin bit sayısıyla doğru orantılıdır; yüksek bit sayısı yüksek hassasiyet sağlar. Bunların dışındaki türler kesirli değerler alamazlar. Örneğin $(C int), yalnızca tamsayı değerler alabilir. $(C nan), "not a number"ın kısaltmasıdır ve $(I geçersiz kesirli sayı değeri) anlamına gelir.
)


$(TABLE $(TABLE_SIZE),
$(HEAD3 Tür, Açıklama, İlk Değer)
$(ROW3 $(C float), 32 bit, $(C float.nan))
$(ROW3 $(C double), 64 bit, $(C double.nan))
$(ROW3 $(C real), en az 64 bit ama donanım desteğine$(BR)göre daha fazla da olabilir, $(C real.nan))
)

$(H5 $(IX char) $(IX wchar) $(IX dchar) Karakter türleri)

$(P
Bu türler harfleri ve metinle ilgili başka her türlü işareti ifade etmek için kullanılırlar:
)

$(TABLE $(TABLE_SIZE),
$(HEAD3 Tür, Açıklama, İlk Değer)
$(ROW3 $(C char), UTF-8 kod birimi, $(C 0xFF))
$(ROW3 $(C wchar), UTF-16 kod birimi, $(C 0xFFFF))
$(ROW3 $(C dchar), UTF-32 kod birimi$(BR)ve Unicode kod noktası, $(C 0x0000FFFF))
)

$(P
Bunlara ek olarak $(I hiçbir türden olmama) kavramını ifade eden $(C void) anahtar sözcüğü vardır.
)

$(P
$(C cent) ve $(C ucent) anahtar sözcükleri, işaretli ve işaretsiz 128 bitlik veri türlerini temsil etmek üzere ilerisi için ayrılmışlardır.
)

$(P
Aksine bir neden bulunmadığı sürece genel bir kural olarak tam değerler için $(C int), kesirli değerler için ise $(C double) türü uygundur.
)

$(H5 Tür nitelikleri)

$(P
Türler hakkında bilgi edinmek amacıyla kullanılan niteliklere türün isminden sonra bir nokta ve nitelik ismiyle erişilir. Örneğin, $(C int)'in $(C sizeof) niteliğine $(C int.sizeof) diye erişilir. Tür niteliklerinin yalnızca bazılarını burada göreceğiz; gerisini sonraki bölümlere bırakacağız:
)

$(UL

$(LI $(IX .stringof) $(C .stringof) türün okunaklı ismidir)

$(LI $(IX .sizeof) $(C .sizeof) türün bayt olarak uzunluğudur (kaç bitten oluştuğunu hesaplamak için bu değeri bir bayttaki bit sayısı olan 8 ile çarpmak gerekir))

$(LI $(IX .min) $(C .min) "en az" anlamına gelen "minimum"un kısaltmasıdır; türün alabileceği en küçük değerdir)

$(LI $(IX .max) $(C .max) "en çok" anlamına gelen "maximum"un kısaltmasıdır; türün alabileceği en büyük değerdir)

$(LI $(IX .init) $(IX ilk değer) $(IX varsayılan değer, tür) $(C .init) "ilk değer" anlamına gelen "initial value"nun kısaltmasıdır; belirli bir tür için özel bir değer belirtilmediğinde kullanılan değer budur)

)

$(P
Bu nitelikleri $(C int) türü üzerinde gösteren bir program şöyle yazılabilir:
)

---
import std.stdio;

void main() {
    writeln("Tür                 : ", int.stringof);
    writeln("Bayt olarak uzunluğu: ", int.sizeof);
    writeln("En küçük değeri     : ", int.min);
    writeln("En büyük değeri     : ", int.max);
    writeln("İlk değeri          : ", int.init);
}
---

$(P
Programın çıktısı:
)

$(SHELL
Tür                 : int
Bayt olarak uzunluğu: 4
En küçük değeri     : -2147483648
En büyük değeri     : 2147483647
İlk değeri          : 0
)

$(H5 $(IX size_t) $(C size_t))

$(P
Programlarda $(C size_t) türü ile de karşılaşacaksınız. $(C size_t) bütünüyle farklı bir tür değildir; ortama bağlı olarak $(C ulong) veya başka bir işaretsiz temel türün takma ismidir. İsmi "size type"tan gelir ve "büyüklük türü" anlamındadır. $(I Adet) gibi saymayla ilgili olan kavramları temsil ederken kullanılır.
)

$(P
Asıl türünün sisteme göre farklı olmasının nedeni, $(C size_t)'nin programın kullanabileceği en büyük bellek miktarını tutabilecek kadar büyük bir tür olmasının gerekmesidir: 32 bitlik sistemlerde $(C uint) ve 64 bitlik sistemlerde $(C ulong). Bu yüzden, 32 bitlik sistemlerdeki en büyük tamsayı türü $(C size_t) değil, $(C ulong)'dur.
)

$(P
Bu türün sizin ortamınızda hangi temel türün takma ismi olduğunu yine $(C .stringof) niteliği ile öğrenebilirsiniz:
)

---
import std.stdio;

void main() {
    writeln(size_t.stringof);
}
---

$(P
Yukarıdaki programı denediğim ortamda şu çıktıyı alıyorum:
)

$(SHELL
ulong
)

$(PROBLEM_TEK

$(P
Diğer türlerin de niteliklerini yazdırın. Notlar:
)

$(UL
$(LI İlerisi için düşünüldükleri için geçersiz olan $(C cent) ve $(C ucent) türlerini hiçbir durumda kullanamazsınız.)

$(LI $(I Hiçbir türden olmamayı) temsil eden $(C void) türünün $(C .min), $(C .max), ve $(C .init) nitelikleri yoktur.)

$(LI Karakter türlerinin $(C .min), $(C .max), ve $(C .init) değerlerinin okunur karakter karşılıkları yoktur.)

$(LI $(C .min) niteliği kesirli sayı türleriyle kullanılamaz. Eğer bu problemde bir kesirli sayı türünü $(C .min) niteliği ile kullanırsanız derleyici bir hata verecektir. Daha sonra $(LINK2 /ders/d/kesirli_sayilar.html, Kesirli Sayılar bölümünde) göreceğimiz gibi, kesirli sayı türlerinin en küçük değeri için $(C .max) niteliğinin eksi işaretlisini kullanmak gerekir (örneğin, $(C -double.max)).)
)

)

Macros:
        SUBTITLE=Temel Türler

        DESCRIPTION=D dilinin temel türleri

        KEYWORDS=d programlama dili ders bölümler öğrenmek tutorial temel türler

TABLE_SIZE=wide

SOZLER=
$(bayt)
$(bit)
$(emekliye_ayrılmıştır)
$(isaretli_tur)
$(isaretsiz_tur)
$(kayan_noktali)
$(mikro_islemci)
$(nitelik)
$(sinif)
$(yapi)
