Ddoc

$(DERS_BOLUMU $(IX const) $(IX immutable) $(IX değişmezlik) Değişmezlik)

$(P
Kavramlar, programlarda kullanılan değişkenlerle temsil edilir. Kavramlar arasındaki etkileşimleri bu değişkenlerin değerlerini değiştirerek sağlarız. Örneğin, aşağıdaki kod bir alışveriş ile ilgili değişkenlerin değerlerini $(I değiştirmektedir):
)

---
    toplamFiyat = fiyatıHesapla(fiyatListesi);
    cüzdandakiMiktar $(HILITE -=) toplamFiyat;
    bakkaldakiMiktar $(HILITE +=) toplamFiyat;
---

$(P
Değer değişiminin bilerek kısıtlanmasına $(I değişmezlik) denir.
)

$(P
Değer değişimi çoğu iş için gerekli olduğundan değişimin bilerek kısıtlanması anlamsız gibi görünebilse de oldukça güçlü ve yararlı bir olanaktır. Değişmezlik kavramı, yazılım dünyası tarafından edinilmiş olan deneyimlere dayanır: Değişmezlik, kodların doğruluğuna ve kolay değiştirilebilmelerine katkı sağlar. Hatta, bazı fonksiyonel programlama dilleri değer değişimini bütünüyle yasaklarlar.
)

$(P
Değişmezliğin getirdiği bazı yararlar şunlardır:
)

$(UL

$(LI Bazı kavramlar zaten $(I değişmezdirler). Örneğin haftadaki gün sayısı 7'dir, matematikteki $(I pi) (π) sabittir, bir programın desteklediği dil sayısı programın çalıştığı sürece değişmeyecektir (örneğin yalnızca Türkçe ve İngilizce'dir), vs.
)

$(LI
Değişmeyen kavramları temsil etmeseler bile, bazı değişkenlerin değerlerinin bir kere ilklendikten sonra değişmesi istenmiyer olabilir ve bunların sonradan değişmeleri programcı hatası olarak kabul edilebilir. Örneğin, yukarıdaki koddaki $(C toplamFiyat)'ın değeri bir kere belirlendikten sonra değişmemelidir.
)

$(LI
Koddaki bütün işlemlerin her değişkeni değiştirebilecek kadar esnek olmaları, hangi işlemlerin hangi değişkenleri değiştirdiklerini fazla serbest bıraktığından kodun okunması ve geliştirilmesi güçleşir.
)

$(LI
İşlevler, parametrelerinin yalnızca bazılarını değişebilen türden tanımladıklarında, hangi parametreleri yalnızca giriş bilgisi olarak kullanacaklarını ve hangilerini yan etkileri olarak değiştireceklerini belirtmiş olurlar.
)

)

$(P
Değişmezlik, hem genel olarak programlamada çok yaygın olduğundan hem de D programcılığında çok kullanıldığından, bu kavramla ilgili aşağıdaki garipliklere göz yumulur:
)

$(UL

$(LI
Şimdiye kadar yazmış olduğumuz programlardan görüldüğü gibi, değişmezlik kesinlikle gerekli değildir.
)

$(LI
$(IX tür nitelendirici) $(IX nitelendirici, tür) Değişmezlik, D'd $(C const) ("sabit, değişmez" anlamına gelen "constant"ın kısası) ve $(C immutable) ("değişebilen" anlamına gelen "mutable"ın karşıt anlamlısı) anahtar sözcükleriyle ifade edilir. Her ne kadar İngilizce'de aynı anlama gelseler de bu anahtar sözcüklerin görevleri farklıdır ve bazı durumlarda birbirleriyle uyumsuzdur. ($(LINK2 /ders/d/islev_parametreleri.html, $(C inout)) ve $(LINK2 /ders/d/es_zamanli_shared.html, $(C shared)) gibi, $(C const) ve $(C immutable) da $(I tür nitelendiricisidir).)
)

$(LI
Bir değişkenin değişmezliğinden bahseden "constant variable" ve "immutable variable" terimleri İngilizce de anlamsızdır ve kulağa yanlış gelir.
)

$(LI
İşlevler değişmezlik kavramını göz önüne almaya zorlanırlar ve bu sayede daha kullanışlı hale gelirler. (Bu zorundalığın işlevden işleve geçmesi bazen bir virüsün yayılmasına benzetilir ve "const-correctness" olarak adlandırılır.)
)

)

$(H5 Değişmezler)

$(P
Kesinlikle değişmeyecek olan değişkenler üç farklı biçimde tanımlanabilirler.
)

$(H6 $(IX enum) $(C enum) değişkenler)

$(P
Bazı sabit değişkenlerin $(C enum) olarak tanımlanabildiklerini $(LINK2 /ders/d/enum.html, $(C enum) bölümünde) görmüştük:
)

---
    enum dosyaİsmi = "liste.txt";
---

$(P
Derleme zamanında hesaplanabildikleri sürece $(C enum) değişkenler işlev çağrıları da dahil olmak üzere daha karmaşık ifadelerle de ilklenebilirler:
)

---
int satırAdedi() {
    return 42;
}

int sütunAdedi() {
    return 7;
}

string isim() {
    return "liste";
}

void main() {
    enum dosyaİsmi $(HILITE =) isim() ~ ".txt";
    enum toplamKare $(HILITE =) satırAdedi() * sütunAdedi();
}
---

$(P
Bunu sağlayan D olanağı, $(LINK2 /ders/d/islevler_diger.html, ilerideki bir bölümde) göreceğimiz $(I derleme zamanında işlev işletme) olanağıdır (CTFE).
)

$(P
Derleyici $(C enum) değişkenlerin değiştirilmelerine izin vermez:
)

---
    ++toplamKare;    $(DERLEME_HATASI)
---

$(P
Değişmezlik kavramını sağlayan çok etkili bir olanak olmasına karşın $(C enum) ancak değerleri derleme zamanında bilinen veya hesaplanabilen sabitler için kullanılabilir.
)

$(P
Bekleneceği gibi, program derlenirken $(C enum) değişkenlerin yerlerine onların değerleri kullanılır. Örneğin, şöyle bir $(C enum) tanımı ve onu kullanan iki ifade olsun:
)

---
    enum i = 42;
    writeln(i);
    foo(i);
---

$(P
Yukarıdaki kod, $(C i)'nin yerine onun değeri olan $(C 42)'nin doğrudan yazılmasının eşdeğeridir:
)

---
    writeln(42);
    foo(42);
---

$(P
$(IX enum, dizilere bedeli) $(IX dizi, enum bedeli) Bir $(C enum) değişkenin yerine değerinin kullanılıyor olması $(C int) gibi basit türler için normal olarak kabul edilmelidir. Ancak, $(C enum) değişkenlerin dizi veya eşleme tablosu olarak kullanılmalarının gizli bir bedeli vardır:
)

---
    enum a = [ 42, 100 ];
    writeln(a);
    foo(a);
---

$(P
$(C a)'nın yerine değerini yerleştirdiğimizde derleyicinin derleyeceği asıl kodun aşağıdaki gibi olduğunu görürüz:
)

---
    writeln([ 42, 100 ]);    // bir dizi oluşturulur
    foo([ 42, 100 ]);        // başka bir dizi oluşturulur
---

$(P
Yukarıdaki koddaki gizli bedel, her ifade için farklı bir dizi oluşturuluyor olmasıdır. Bu yüzden, birden fazla yerde kullanılacak olan dizilerin ve eşleme tablolarının $(C immutable) değişkenler olarak tanımlanmaları çoğu duruma daha uygundur.
)

$(H6 $(IX değişken, const) $(C const) değişkenler)

$(P
$(C enum) gibi, bu anahtar sözcük de bir değişkenin değerinin değişmeyeceğini bildirir. $(C enum)'dan farkı, $(C const) değişkenlerin adresleri olan normal değişkenler olmaları ve ilk değerlerini çalışma zamanında da alabilmeleridir.
)

$(P
Derleyici $(C const) değişkenlerin değiştirilmelerine izin vermez:
)

---
    $(HILITE const) yarısı = toplam / 2;
    yarısı = 10;    $(DERLEME_HATASI)
---

$(P
Aşağıdaki program $(C enum) ve $(C const) anahtar sözcüklerinin kullanımlarının farklarını gösteriyor. Tuttuğu sayıyı kullanıcının tahmin etmesini bekleyen bu programda tutulan sayı derleme zamanında bilinemeyeceğinden $(C enum) olarak tanımlanamaz. Ancak, bir kere seçildikten sonra değerinin değişmesi istenmeyeceğinden ve hatta değişmesi bir hata olarak kabul edileceğinden bu değişkenin $(C const) olarak işaretlenmesi uygun olur.
)

$(P
Aşağıdaki program kullanıcının tahminini okurken yine bir önceki bölümde tanımladığımız $(C sayıOku) işlevinden yararlanıyor:
)

---
import std.stdio;
import std.random;

int sayıOku(string mesaj) {
    int sayı;
    write(mesaj, "? ");
    readf(" %s", &sayı);
    return sayı;
}

void main() {
    enum enAz = 1;
    enum enÇok = 10;

    $(HILITE const) sayı = uniform(enAz, enÇok + 1);

    writefln("%s ile %s arasında bir sayı tuttum.",
             enAz, enÇok);

    auto doğru_mu = false;
    while (!doğru_mu) {
        $(HILITE const) tahmin = sayıOku("Tahmininiz");
        doğru_mu = (tahmin == sayı);
    }

    writeln("Doğru!");
}
---

$(P
Gözlemler:
)

$(UL

$(LI $(C enAz)'ın ve $(C enÇok)'un değerleri programın derlenmesi sırasında bilindiklerinden ve bir anlamda bu programın davranışının değişmez parçaları olduklarından $(C enum) olarak tanımlanmışlardır.
)

$(LI Rasgele seçilmiş olan $(C sayı) değerinin ve kullanıcıdan okunan her $(C tahmin) değerinin programın işleyişi sırasında değişmeleri doğru olmayacağından onlar $(C const) olarak tanımlanmışlardır.
)

$(LI O değişkenlerin tanımları sırasında türlerinin açıkça belirtilmediğine dikkat edin. $(C auto)'da olduğu gibi, $(C enum) ve $(C const) anahtar sözcükleri de türün sağ tarafın değerinden çıkarsanması için yeterlidir.
)

)

$(P
Program içinde açıkça $(C const(int)) diye parantezle yazılması gerekmese de $(C const) türün bir parçasıdır. Aşağıdaki program üç farklı biçimde tanımlanmış olan değişkenlerin türlerinin tam isimlerinin aynı olduklarını gösteriyor:
)

---
import std.stdio;

void main() {
    const      çıkarsanarak = 0;
    const int  türüyle      = 1;
    const(int) tamOlarak    = 2;

    writeln(typeof(çıkarsanarak).stringof);
    writeln(typeof(türüyle).stringof);
    writeln(typeof(tamOlarak).stringof);
}
---

$(P
Üçünün de asıl tür ismi $(C const)'ı da içerir ve parantezlidir:
)

$(SHELL
const(int)
const(int)
const(int)
)

$(P
Parantezlerin içindeki tür önemlidir. Bunu aşağıda dilimin veya elemanlarının değişmezliği konusunda göreceğiz.
)

$(H6 $(IX değişken, immutable) $(C immutable) değişkenler)

$(P
Değişken tanımında $(C immutable) anahtar sözcüğü $(C const) ile aynıdır. $(C immutable) değişkenler değiştirilemezler:
)

---
    $(HILITE immutable) yarısı = toplam / 2;
    yarısı = 10;    $(DERLEME_HATASI)
---

$(P

Programın başka tarafları özellikle $(C immutable) gerektirmediğinde, değişkenleri $(C const) veya $(C immutable) olarak tanımlayabilirsiniz. Bir işlevin özellikle $(C immutable) gerektirdiği durumda ise o parametreye gönderilecek olan değişkenin de $(C immutable) olarak tanımlanmış olması gerekir. Bunu aşağıda göreceğiz.
)

$(H5 Parametreler)

$(P
Sonraki iki bölümde göreceğimiz gibi, işlevler parametrelerinde değişiklik yapabilirler. Örneğin, parametre olarak gönderilmiş olan dilimlerin elemanlarını değiştirebilirler.
)

$(P
$(LINK2 /ders/d/dilimler.html, Başka Dizi Olanakları bölümünden) hatırlayacağınız gibi, dilimler kendi elemanlarına sahip değillerdir, o elemanlara yalnızca erişim sağlarlar. Belirli bir anda aynı elemana erişim sağlamakta olan birden fazla dilim bulunabilir.
)

$(P
Bu başlık altındaki örneklerde dilimlerden yararlanıyor olsam da burada anlatılanlar eşleme tabloları için de geçerlidir çünkü onlar da $(I referans türleridir).
)

$(P
İşlev parametresi olan bir dilim, işlevin çağrıldığı yerdeki dilimin kendisi değil, bir $(I kopyasıdır). (Yalnızca dilim değişken kopyalanır, elemanları değil.)
)

---
import std.stdio;

void main() {
    int[] dilim = [ 10, 20, 30, 40 ];  // 1
    yarıla(dilim);
    writeln(dilim);
}

void yarıla(int[] sayılar) {           // 2
    foreach (ref sayı; sayılar) {
        sayı /= 2;
    }
}
---

$(P
Yukarıdaki $(C yarıla) işlevinin işletildiği sırada aynı dört elemana erişim sağlamakta olan iki farklı dilim vardır:
)

$(OL

$(LI $(C main)'in içinde tanımlanmış olan ve $(C yarıla)'ya parametre olarak gönderilen $(C dilim) isimli dilim
)

$(LI $(C yarıla)'nın parametre değeri olarak almış olduğu ve $(C main)'deki dilimle aynı dört elemana erişim sağlamakta olan $(C sayılar) isimli dilim
)

)

$(P
$(C foreach) döngüsünde $(C ref) anahtar sözcüğü de kullanılmış olduğundan o dört elemanın değerleri yarılanmış olur:
)

$(SHELL
[5, 10, 15, 20]
)

$(P
Bu örnekte de görüldüğü gibi, $(C yarıla) gibi işlevlerin kendilerine gönderilen dilimlerin elemanlarını değiştirebilmeleri kullanışlıdır çünkü zaten eleman değiştirmek için yazılmışlardır.
)

$(P
Derleyici, $(C const) değişkenlerin böyle işlevlere gönderilmelerine izin vermez:
)

---
    $(HILITE const) int[] dilim = [ 10, 20, 30, 40 ];
    yarıla(dilim);    $(DERLEME_HATASI)
---

$(P
Derleme hatası, $(C const(int[])) türündeki bir değişkenin $(C int[]) türündeki bir parametre değeri olarak kullanılamayacağını bildirir:
)

$(SHELL
Error: function deneme.yarıla ($(HILITE int[]) sayılar) is not callable
using argument types ($(HILITE const(int[])))
)

$(H6 $(IX parametre, const) $(C const) parametreler)

$(P
$(C const) değişkenlerin $(C yarıla)'da olduğu gibi parametrelerinde değişiklik yapan işlevlere gönderilmelerinin engellenmesi önemlidir. Ancak, parametrelerinde değişiklik yapmayan ve hatta yapmaması gereken aşağıdaki $(C yazdır) gibi işlevlere gönderilememeleri büyük bir kısıtlama olarak görülmelidir:
)

---
import std.stdio;

void main() {
    const(int[]) dilim = [ 10, 20, 30, 40 ];
    yazdır(dilim);    $(DERLEME_HATASI)
}

void yazdır(int[] dilim) {
    writefln("%s eleman: ", dilim.length);

    foreach (i, eleman; dilim) {
        writefln("%s: %s", i, eleman);
    }
}
---

$(P
Elemanların $(C const) olarak tanımlanmış olmaları, onların yazdırılmalarına engel olmamalıdır. $(C const) parametreler bu konuda yararlıdırlar.  ($(C const) kavramını böylece doğru kullandığı düşünülen işlevlerin "const konusunda doğru" anlamında "const-correct" oldukları söylenir.)
)

$(P
$(C const) anahtar sözcüğü bir değişkenin $(I belirli bir referans) (örneğin dilim) yoluyla değiştirilmeyeceğini belirler. Parametreyi $(C const) olarak işaretlemek, o dilimin elemanlarının işlev içerisinde değiştirilemeyeceğini garanti eder. Böyle bir garanti sağlandığı için program artık derlenir:
)

---
    yazdır(dilim);    // şimdi derlenir
// ...
void yazdır($(HILITE const) int[] dilim) {
    // ...
}
---

$(P
İşlev nasıl olsa değiştirmeyeceğine söz vermiş olduğundan, hem $(I değişebilen), hem $(C const), hem de $(C immutable) değişkenler o işlevin $(C const) parametresi olarak gönderilebilirler:
)

---
    $(HILITE int[]) değişebilenDilim = [ 7, 8 ];
    yazdır(değişebilenDilim);    // derlenir

    $(HILITE const int[]) dilim = [ 10, 20, 30, 40 ];
    yazdır(dilim);               // derlenir

    $(HILITE immutable int[]) immDilim = [ 1, 2 ];
    yazdır(immDilim);            // derlenir
---

$(P
$(IX const-correct) $(IX virüs, değişmezlik) İşlev tarafından değiştirilmediği halde $(C const) olarak tanımlanmayan bir parametre, işlevin kullanışlılığını düşürür. Böyle işlevlerin "const-correct" olmadıkları söylenir.
)

$(P
$(C const) parametrelerin başka bir yararı, programcıya verdikleri yararlı bilgidir: Değişkenin işlev tarafından değiştirilmeyeceğini bilmek kodun anlaşılırlığını arttırır.
)

$(P
$(C const) parametrelerin değişebilen, $(C const), ve $(C immutable) değişkenleri kabul edebilmelerinin ilginç bir etkisi vardır. Bunu aşağıdaki "$(C const) parametre mi, $(C immutable) parametre mi?" başlığı altında göreceğiz.
)

$(H6 $(IX in, değişmezlik) $(C in) parametreler)

$(P
Bir sonraki bölümde göreceğimiz gibi, $(C in) hem $(C const) anlamını içerir, hem de $(C -preview=in) derleyici seçeneği ile kullanıldığında daha yararlıdır. Bu yüzden, $(C const) parametreler yerine $(C in) parametreler kullanmanızı öneririm.
)

$(H6 $(IX parametre, immutable) $(C immutable) parametreler)

$(P
Hem $(I değişebilen), hem $(C const), hem de $(C immutable) değişkenler alabildiklerinden $(C const) parametrelerin esnek olduklarını söyleyebiliriz.
)

$(P
Öte yandan, bir parametrenin $(C immutable) olarak işaretlenmesi, asıl değişkenin de $(C immutable) olması şartını getirir. Bu açıdan bakıldığında $(C immutable) parametreler işlevin çağrıldığı nokta üzerinde kuvvetli bir talepte bulunmaktadırlar:
)

---
void birİşlem($(HILITE immutable) int[] dilim) {
    // ...
}

void main() {
    immutable int[] değişmezDilim = [ 1, 2 ];
    int[] değişebilenDilim = [ 8, 9 ];

    birİşlem(değişmezDilim);       // bu derlenir
    birİşlem(değişebilenDilim);    $(DERLEME_HATASI)
}
---

$(P
O yüzden $(C immutable) parametreleri ancak gerçekten gereken durumlarda düşünmenizi öneririm. Şimdiye kadar öğrendiklerimiz arasında $(C immutable) parametreler yalnızca dizgi türlerinde üstü kapalı olarak geçerler. Bunu biraz aşağıda göstereceğim.
)

$(P
$(C const) veya $(C immutable) olarak işaretlenmiş olan parametrelerin, işlevin çağrıldığı yerdeki asıl değişkeni değiştirmeme sözü verdiklerini gördük. Bu konu yalnızca referans türünden olan değişkenlerle ilgilidir.
)

$(P
Referans ve değer türlerini bir sonraki bölümde daha ayrıntılı olarak göreceğiz. Bu bölüme kadar gördüğümüz türler arasında dilimler ve eşleme tabloları referans türleri, diğerleri ise değer türleridir.
)

$(H6 $(IX parametre, const veya immutable) $(C const) parametre mi, $(C immutable) parametre mi?)

$(P
$(I Not: $(C in) parametreler $(C const) anlamını içerdiklerinden, bu bölüm $(C in) parametrelerle de ilgilidir.
)
)

$(P
Yukarıdaki başlıklara bakıldığında esneklik getirdiği için $(C const) belirtecinin yeğlenmesinin doğru olacağı sonucuna varılabilir. Bu her zaman doğru değildir.
)

$(P
Parametre tanımındaki $(C const) belirteci, asıl değişkenin $(I değişebilen) mi, $(C const) mı, yoksa $(C immutable) mı olduğu bilgisini işlev içerisinde belirsiz hale getirir. Bunu derleyici de bilemez.
)

$(P
Bunun bir etkisi, $(C const) parametrelerin $(C immutable) parametre alan başka işlevlere doğrudan gönderilemeyecekleridir. Örneğin, değişken $(C main) içinde her ne kadar $(C immutable) olarak tanımlanmış bile olsa, aşağıdaki koddaki $(C foo) işlevi $(C const) parametresini $(C bar)'a gönderemez:
)

---
void main() {
    /* Asıl değişken immutable */
    $(HILITE immutable) int[] dilim = [ 10, 20, 30, 40 ];
    foo(dilim);
}

/* Daha kullanışlı olabilmek için parametresini const olarak
 * alan bir işlev. */
void foo(const int[] dilim) {
    bar(dilim);    $(DERLEME_HATASI)
}

/* Parametresini belki de geçerli bir nedenle immutable olarak
 * alan bir işlev. */
void bar(immutable int[] dilim) {
    // ...
}
---

$(P
$(C bar), parametresinin $(C immutable) olmasını şart koşmaktadır. Öte yandan, $(C foo)'nun $(C const) parametresi olan $(C dilim)'in aslında $(C immutable) bir değişkene mi yoksa değişebilen bir değişkene mi bağlı olduğu bilinemez.
)

$(P
$(I Not: Yukarıdaki kullanıma bakıldığında $(C main) içindeki asıl değişkenin $(C immutable) olduğu açıktır. Buna rağmen, derleyici her işlevi ayrı ayrı derlediğinden $(C foo)'nun $(C const) parametresinin işlevin her çağrıldığı noktada aslında $(C immutable) olduğunu bilmesi olanaksızdır. Derleyicinin gözünde $(C dilim) değişebilen de olabilir $(C immutable) da.
)
)

$(P
Böyle bir durumda bir çözüm, $(C bar)'ı parametrenin değişmez bir kopyası ile çağırmaktır:
)

---
void foo(const int[] dilim) {
    bar(dilim$(HILITE .idup));
}
---

$(P
Bu durumda kodun derlenmesi sağlanmış olsa da, asıl değişkenin zaten $(C immutable) olduğu durumda bile kopyasının alınıyor olmasının gereksiz bir bedeli olacaktır.
)

$(P
Bütün bunlara bakıldığında belki de $(C foo)'nun parametresini $(C const) olarak almasının her zaman için doğru olmadığı düşünülebilir. Çünkü parametresini baştan $(C immutable) olarak seçmiş olsa kod kopyaya gerek kalmadan derlenebilir:
)

---
void foo(immutable int[] dilim) {  // Bu sefer immutable
    bar(dilim);    // Artık kopya gerekmez
}
---

$(P
Ancak, bir üstteki başlıkta belirtildiği gibi, asıl değişkenin $(C immutable) olmadığı durumlarda $(C foo)'nun çağrılabilmesi için bu sefer de $(C .idup) ile kopyalanması gerekecekti:
)

---
    foo(değişebilenDilim$(HILITE .idup));
---

$(P
Görüldüğü gibi, değişmeyecek olan parametrenin türünün $(C const) veya $(C immutable) olarak belirlenmesinin kararı kolay değildir.
)

$(P
İleride göreceğimiz şablonlar bu konuda da yararlıdırlar. Aşağıdaki kodları kitabın bu aşamasında anlamanızı beklemesem de parametrenin $(I değişebilen), $(C const), veya $(C immutable) olması kararını ortadan kaldırdığını belirtmek istiyorum. Aşağıdaki $(C foo), yalnızca asıl değişken $(C immutable) olmadığında kopya bedeli öder; $(C immutable) değişkenler kopyalanmazlar:
)

---
import std.conv;
// ...

/* Şablon olduğu için hem değişebilen hem de immutable
 * değişkenlerle çağrılabilir. */
void foo(T)(T[] dilim) {
    /* Asıl değişken zaten immutable olduğunda 'to' ile
     * kopyalamanın bedeli yoktur. */
    bar(to!(immutable T[])(dilim));
}
---

$(H5 $(IX ilkleme, değişmezlik) İlkleme)

$(P
Değişimin engellenmesinin, değişken ilk değerlerinin az da olsa karmaşık ifadelerden oluştuğu durumlarda kısıtlayıcı olduğu düşünülebilir. Örneğin, aşağıdaki $(C meyveler) dizisinin elemanlarının, $(C turunçgilEklensin_mi) değişkeninin değerine göre belirlenmesi istenmiştir, ancak kod dizi $(C const) olduğundan derlenemez:
)

---
    $(HILITE const) meyveler = [ "elma", "armut" ];

    if (turunçgilEklensin_mi) {
        meyveler ~= [ "portakal" ];    $(DERLEME_HATASI)
    }
---

$(P
Değişkeni örneğin $(C auto) ile tanımlamak kodun derlenmesi için yeterli olsa da, daha iyi bir yöntem, $(C const)'ı kullanmaya devam etmek ama ilkleme kodunu bir işleve taşımaktır:
)

---
bool turunçgilEklensin_mi;

string[] meyvelerYap() {
    auto sonuç = [ "elma", "armut" ];

    if (turunçgilEklensin_mi) {
        sonuç ~= [ "portakal" ];
    }

    return sonuç;
}

void main() {
    $(HILITE const) meyveler = meyvelerYap();
}
---

$(P
$(C sonuç) dizisinin değişebilen türden olduğu halde $(C meyveler)'in istendiği gibi $(C const) olabildiğine dikkat edin. Kodun bir işleve taşınmasının mümkün olmadığı veya güçlük doğurduğu durumlarda $(LINK2 /ders/d/kapamalar.html, isimsiz işlevler) kullanılabilir:
)

---
    const meyveler = $(HILITE {)
      // 'meyvelerYap()' işlevinin içeriğinin aynısı
      auto sonuç = [ "elma", "armut" ];

      if (turunçgilEklensin_mi) {
          sonuç ~= [ "portakal" ];
      }

      return sonuç;
    $(HILITE })();
---

$(P
İsimsiz işlev, işaretlenmiş olarak gösterilen küme parantezleri arasında tanımlanmıştır ve sondaki işlev çağrı parantezleriyle işletilmektedir. Sonuçta $(C meyveler) değişkeni, istendiği gibi $(C const) olarak tanımlanabilmiştir.
)

$(P
$(IX shared static this, değişmezlik) $(C shared static this) (ve $(C static this)) özel ilkleme bloklarında $(C const) ve $(C immutable) değişkenlere doğrudan değer atanabilir. Bu bloklar özellikle modül düzeyinde (işlevlerin dışında) tanımlanmış olan değişkenlerin ilklenmeleri için kullanılırlar:
)

---
immutable int[] i;

shared static this() {
    // 'const' ve 'immutable' modül değişkenleri bu blok içinde
    // değiştirilebilirler:
    i ~= 43;

    // Değişkenler programın geri kalanında yine de 'const' veya
    // 'immutable' olarak tanımlanmış gibi kullanılırlar.
}
---

$(P
$(C shared static this) blokları $(C main) işlevinden önce işletilirler.
)

$(H5 Bütün dilime karşılık elemanlarının değişmezliği)

$(P
$(C const) bir dilimin türünün $(C .stringof) ile $(C const(int[])) olarak yazdırıldığını yukarıda gördük. $(C const)'tan sonra kullanılan parantezlerden anlaşılabileceği gibi, değişmez olan dilimin bütünüdür; o dilimde hiçbir değişiklik yapılamaz. Örneğin dilime eleman eklenemez, dilimden eleman çıkartılamaz, var olan elemanların değerleri değiştirilemez, veya dilimin başka elemanları göstermesi sağlanamaz:
)

---
    const int[] dilim = [ 1, 2 ];
    dilim ~= 3;                    $(DERLEME_HATASI)
    dilim[0] = 3;                  $(DERLEME_HATASI)
    dilim.length = 1;              $(DERLEME_HATASI)

    const int[] başkaDilim = [ 10, 11 ];
    dilim = başkaDilim;            $(DERLEME_HATASI)
---

$(P
Değişmezliğin bu derece ileri götürülmesi bazı durumlara uygun değildir. Çoğu durumda önemli olan, yalnızca elemanların değiştirilmeyecekleri güvencesidir. Dilim nasıl olsa elemanlara erişim sağlayan bir olanak olduğundan o elemanlar değiştirilmedikleri sürece dilimin kendisinde oluşan değişiklikler bazı durumlarda önemli değildir.
)

$(P
Bir dilimin yalnızca elemanlarının değişmeyeceği, $(C const)'tan sonraki parantezin yalnızca elemanın türünü içermesi ile sağlanır. Yukarıdaki kod buna uygun olarak değiştirilirse artık yalnızca elemanı değiştiren satır derlenemez; dilimin kendisi değiştirilebilir:
)

---
    const$(HILITE (int))[] dilim = [ 1, 2 ];
    dilim ~= 3;                    // şimdi derlenir
    dilim[0] = 3;                  $(DERLEME_HATASI)
    dilim.length = 1;              // şimdi derlenir

    const int[] başkaDilim = [ 10, 11 ];
    dilim = başkaDilim;            // şimdi derlenir
---

$(P
Birbirlerine çok yakın olan bu söz dizimlerini şöyle karşılaştırabiliriz:
)

---
    const int[]  a = [1]; /* Ne elemanları ne kendisi
                             değiştirilebilen dilim */

    const(int[]) b = [1]; /* Üsttekiyle aynı anlam */

    const(int)[] c = [1]; /* Elemanları değiştirilemeyen ama
                             kendisi değiştirilebilen dilim */
---

$(P
Daha önceki bölümlerde bu konuyla üstü kapalı olarak karşılaştık. Hatırlarsanız, dizgi türlerinin asıl türlerinin $(C immutable) olduklarından bahsetmiştik:
)

$(UL
$(LI $(C string), $(C immutable(char)[])'ın takma ismidir)
$(LI $(C wstring), $(C immutable(wchar)[])'ın takma ismidir)
$(LI $(C dstring), $(C immutable(dchar)[])'ın takma ismidir)
)

$(P
Benzer şekilde, dizgi hazır değerleri de değişmezdirler:
)

$(UL
$(LI $(STRING "merhaba"c) hazır dizgisinin türü $(C string)'dir)
$(LI $(STRING "merhaba"w) hazır dizgisinin türü $(C wstring)'dir)
$(LI $(STRING "merhaba"d) hazır dizgisinin türü $(C dstring)'dir)
)

$(P
Bunlara bakarak D dizgilerinin normalde $(I $(C immutable) karakterlerden) oluştuklarını söyleyebiliriz.
)

$(H6 $(IX değişmezlik, geçişli) $(C const) ve $(C immutable) geçişlidir)

$(P
Yukarıdaki $(C a) ve $(C b) dilimlerinin kod açıklamalarında da değinildiği gibi, o dilimlerin ne kendileri ne de elemanları değiştirilebilir.
)

$(P
Bu, ilerideki bölümlerde göreceğimiz $(LINK2 /ders/d/yapilar.html, yapılar) ve $(LINK2 /ders/d/siniflar.html, sınıflar) için de geçerlidir. Örneğin, $(C const) olan bir yapı değişkeninin bütün üyeleri de $(C const)'tır ve $(C immutable) olan bir yapı değişkeninin bütün üyeleri de $(C immutable)'dır. (Aynısı sınıflar için de geçerlidir.)
)

$(H6 $(IX .dup) $(IX .idup) $(C .dup) ve $(C .idup))

$(P
Karakterleri değişmez olduklarından dizgiler işlevlere parametre olarak geçirilirken uyumsuz durumlarla karşılaşılabilir. Bu durumlarda dizilerin $(C .dup) ve $(C .idup) nitelikleri yararlıdır:
)

$(UL
$(LI $(C .dup) dizinin değişebilen bir kopyasını oluşturur; ismi, "kopyasını al" anlamındaki "duplicate"ten gelir)
$(LI $(C .idup) dizinin değişmez bir kopyasını oluşturur; ismi "immutable duplicate"ten gelir)
)

$(P
Örneğin, parametresinin programın çalışması süresince kesinlikle değişmeyecek olmasını isteyen ve bu yüzden onu $(C immutable) olarak belirlemiş olan bir işlevi $(C .idup) ile alınan bir kopya ile çağırmak gerekebilir:
)

---
void foo($(HILITE string) dizgi) {
    // ...
}

void main() {
    char[] selam;
    foo(selam);                $(DERLEME_HATASI)
    foo(selam$(HILITE .idup));           // ← derlenir
}
---


$(H5 Nasıl kullanmalı)

$(UL

$(LI
Genel bir kural olarak, olabildiği kadar değişmezliği yeğleyin.
)

$(LI
Değişken tanımlarken, programın çalışması sırasında kesinlikle değişmeyecek olan ve değerleri derleme zamanında bilinen veya hesaplanabilen değerleri $(C enum) olarak tanımlayın. Örneğin, dakikadaki saniye sayısı değişmez:

---
    enum int dakikaBaşınaSaniye = 60;
---

$(P
Türün sağ taraftan çıkarsanabildiği durumlarda değişkenin türü belirtilmeyebilir:
)

---
    enum dakikaBaşınaSaniye = 60;
---

)

$(LI
$(C enum) dizisi ve $(C enum) eşleme tablosu kullanmanın gizli bedelini göz önünde bulundurun. Programda birden fazla yerde kullanıldıklarında onları $(C immutable) değişkenler olarak tanımlayın.
)

$(LI
Kesinlikle değişmeyecek olan ama değerleri derleme zamanında bilinmeyen veya hesaplanamayan değişkenleri $(C const) olarak tanımlayın. Türün belirtilmesi yine isteğe bağlıdır:

---
    const tahmin = sayıOku("Tahmininiz");
---

)

$(LI
Parametre tanımlarken, eğer işlev parametrede bir değişiklik yapmayacaksa parametreyi $(C in) olarak tanımlayın. Öyle yaptığınızda parametreyi değiştirmeme sözü verdiğinizden, $(I değişebilen), $(C const), ve $(C immutable) değişkenleri o işleve gönderebilirsiniz:

---
void foo(in char[] dizgi) {
    // ...
}

void main() {
    char[] değişebilenDizgi;
    string immutableDizgi;

    foo(değişebilenDizgi);  // ← derlenir
    foo(immutableDizgi);    // ← derlenir
}
---

)

$(LI
Bu kitaptaki çoğu örneğin aksine, parametrelerin hangi bölümlerinin değişmez olduğunu belirtin:

---
// Elemanlar değişemez, dilim değişebilir
void yazdır_1(const$(HILITE $(PARANTEZ_AC))int$(HILITE $(PARANTEZ_KAPA))[] dilim) {
    // ...
}

// Ne dilim ne de elemanlar değişebilir
void yazdır_2(const$(HILITE $(PARANTEZ_AC))int[]$(HILITE $(PARANTEZ_KAPA)) dilim) {
    // ...
}

// yazdır_2() ile aynı
// (bu kitaptaki örneklerin aksine, bu yöntemden kaçının)
void yazdır_3(const int[] dilim) {
    // ...
}
---
)

$(LI
$(C const) değişkenleri $(C immutable) parametre alan işlevlere gönderemeyeceğinizi unutmayın. Bu konunun ayrıntılarını yukarıdaki "$(C const) parametre mi, $(C immutable) parametre mi?" başlığında gördük.
)

$(LI
Eğer parametrede bir değişiklik yapacaksanız o parametreyi değişebilen şekilde tanımlayın ($(C in), $(C const), veya $(C immutable) olarak tanımlansa zaten derleyici izin vermez):

---
import std.stdio;

void tersÇevir(dchar[] dizgi) {
    foreach (i; 0 .. dizgi.length / 2) {
        immutable geçici = dizgi[i];
        dizgi[i] = dizgi[$ - 1 - i];
        dizgi[$ - 1 - i] = geçici;
    }
}

void main() {
    dchar[] selam = "merhaba"d.dup;
    tersÇevir(selam);
    writeln(selam);
}
---

$(P
Çıktısı:
)

$(SHELL
abahrem
)

)

)

$(H5 Özet)

$(UL

$(LI $(C enum) değişkenler, değerleri derleme zamanında bilinen ve kesinlikle değişmeyecek olan kavramları temsil ederler.)

$(LI $(C enum) dizilerin ve $(C enum) eşleme tablolarının kodda geçtikleri her noktada bellek ayrılması gibi gizli bir bedelleri vardır. Diziler ve eşleme tabloları için $(C immutable) belirtecini kullanın.)

$(LI $(C const) parametreler yerine $(C in) parametreleri yeğleyin.)

$(LI $(C const) ve $(C immutable) değişkenler, değerleri derleme zamanında bilinemeyen ama kesinlikle değişmeyecek olan kavramları temsil ederler.)

$(LI $(C const) bir parametre, işlev tarafından değiştirilemez. O yüzden, hem $(I değişebilen), hem $(C const), hem de $(C immutable) değişkenler o parametrenin değeri olarak kullanılabilirler.)

$(LI $(C immutable) parametreler, işlevin özellikle $(C immutable) olmasını talep ettiği parametrelerdir. İşlev çağrılırken bu parametrelere karşılık yalnızca $(C immutable) değişkenler gönderilebilir.)

$(LI $(C const(int[])), dilimin de elemanlarının da değişmez olduklarını belirler.)

$(LI $(C const(int)[]), yalnızca elemanların değişmez olduklarını belirler.)

)

Macros:
        SUBTITLE=Değişmezlik

        DESCRIPTION=D dilinde değişmezlik kavramlarıyla ilgili olan const ve immutable anahtar sözcüklerinin tanıtılması

        KEYWORDS=d programlama dili ders dersler öğrenmek tutorial const immutable sabit değişmez

SOZLER=
$(degisken)
$(degismez)
$(fonksiyonel_programlama)
$(islev)
$(kapsam)
$(parametre)
$(sabit)
$(takma_isim)
$(tur_nitelendirici)
